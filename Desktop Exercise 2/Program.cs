﻿using System;
using System.Linq;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      var arr = ArrayFactory.GetArray();

      
      Console.WriteLine("**** OutputArray ****");
      ArrayFactory.OutputArray(arr);
      //Console.ReadLine();

      Console.WriteLine("\r\n**** AverageArrayValue ****\n");
      var average = ArrayFactory.AverageArrayValue(arr);
      Console.Write("\t {0:C}\n", average);
      //Console.ReadLine();

      Console.WriteLine("\r\n**** MinArrayValue ****\n");
      var min = ArrayFactory.MinArrayValue(arr);
      Console.WriteLine("\t {0:C}\n", min);

      Console.WriteLine("\r\n**** MaxArrayValue ****\n");
      var max = ArrayFactory.MaxArrayValue(arr);
      Console.WriteLine("\t {0:C}\n", max);
      //Console.ReadLine();

      Console.WriteLine("\r\n**** SortArrayAsc ****\nThese are the beautifully sorted values: ");
      var sorted = ArrayFactory.SortArrayAsc(arr);
      ArrayFactory.OutputArray(sorted);

      Console.ReadLine();
      
      Console.Read();
    }
  }
}
