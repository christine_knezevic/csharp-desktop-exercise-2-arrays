﻿using System;
using System.Text;
using System.Linq;


namespace Desktop_Exercise_2
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of 10 random numbers
    /// </summary>
    /// <returns></returns>
    public static decimal[] GetArray()
    {
      Console.WriteLine("Please enter array length");
      int arraylength = Int32.Parse(Console.ReadLine());
      decimal[] arr = new decimal[arraylength];
      Console.WriteLine("What minimum value do you want to use?");
      int arraymin = Int32.Parse(Console.ReadLine());
      Console.WriteLine("What maximum value do you want to use?");
      int arraymax = Int32.Parse(Console.ReadLine());
      //var arr = new decimal[10];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween((decimal)arraymin, (decimal)arraymax);
      }

      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    public static void OutputArray(decimal[] arr)
    {
      foreach (var i in arr)
      {
        Console.Write("\t {0:C}\n", i);
      }
      //throw new NotImplementedException();
    }
    
    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    public static decimal AverageArrayValue(decimal[] arr)
    {
      decimal average = arr.Average();
      //Console.Write("This is the average: \n\t{0}\n", average);
      return average;
    }


    
    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    public static decimal MaxArrayValue(decimal[] arr)
    {
      decimal max = arr.Max();
      return max;
      //Console.Write("This is the highest value: \n\t{0}\n", max);
      //throw new NotImplementedException();
    }
    
    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    public static decimal MinArrayValue(decimal[] arr)
    {
      decimal low = arr.Min();
      return low;
      //Console.Write("This is the lowest value: \n\t{0}\n", low);
      //throw new NotImplementedException();
    }
    
    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    public static decimal[] SortArrayAsc(decimal[] arr)
    {
      Array.Sort(arr);
      return arr; 
      //foreach (decimal value in arr)
      //{
        
      //  //Console.Write(value);
      //  Console.Write("\t{0}\n", value);
      //}
      //  Console.WriteLine();
    }
      //decimal sorted = Array.Sort<decimal, decimal>(arr);
      //Console.Write("These are the beautifully sorted values: {0}\n", sorted);
      //throw new NotImplementedException();

    
    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}
